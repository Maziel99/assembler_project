    
    ; Programista: Maciej Zielinski
    ; Tester;      Adrian Wolan
    
    
    [bits 32]

extern _printf
extern _exit

a equ 12
b equ 9

section .data
    formatWynik db "NWD(%d, %d) = %d", 0xA, 0
    formatBlad db "NWD jest wyliczane tylko dla liczb calkowitych dodatnich", 0xA, 0
    
section .text

global _main
_main:
    mov ebp, esp    ; for correct debugging
    
    mov eax, a
    mov ecx, b
    
    cmp eax, 0
    jle nwdBlad
    
    cmp ecx, 0
    jle nwdBlad
    
    nwd:
        cmp eax, ecx
        je nwdKoniec ; a rowne b to przenosi do nwdKoniec
        jl nwd2 ; a mniejsze od b  to przenosi do nwd2
        
        sub eax, ecx ; odejmowanie a -= b
        jmp nwd
    
    nwd2:
        sub ecx, eax ; odejmowanie b -= a
        jmp nwd
    
    nwdKoniec:
        push eax        ; "pcha" wartosci na stos
        push b
        push a
        push formatWynik
        call _printf
        add esp, 4 * 4  ; wskaznik stosu na ret
        
        jmp Koniec      ; przenosi do Koniec
    
    nwdBlad:
        push formatBlad
        call _printf
        add esp, 1 * 4  ; wskaznik stosu na ret
    
    Koniec:
        push 0
        call _exit  ; exit