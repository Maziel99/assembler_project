
      ; Programista: Maciej Zielinski
      ; Tester:      Adrian Wolan



            [bits 32]

a equ 12        ; miejsce ustawiania wartosci do obliczenia
b equ 9

mov eax, a     ; przenoszenie warto�ci
mov ecx, b     ; jw


cmp eax, 0     ; porownywanie wartosci do zera
jle nwdBlad    ; przenosi do nwdBlad, gdy warto�� eax jest 0  lub mniej

cmp ecx, 0     ; porownywanie wartosci do zera
jle nwdBlad    ; przenosi do nwdBlad, gdy warto�� ecx jest 0  lub mniej




nwd:
    cmp eax, ecx            ; porownuje eax z ecx
    je nwdKoniec            ; a rowne b to przenosi do nwdKoniec
    jl nwd2                 ; a mniejsze od b  to przenosi do nwd2

    sub eax, ecx            ; odejmowanie a -= b
    jmp nwd                 ; przenosi do nwd

nwd2:

    sub ecx, eax            ; odejmowanie b -= a
    jmp nwd                 ; przenosi do nwd

nwdKoniec:

    push eax                ; zapisanie wartosci do stosu
    push b                  ; jw
    push a                  ; jw
    call wynik              ; wywoluje wynik

formatWynik: db "NWD(%d, %d) = %d", 0xA, 0     ; wypisanie poprawnego wyniku

wynik:

    call [ebx + 3 * 4]                         ; wywoluje printf w asmloader
    add esp, 4 * 4                             ; ustawia wskaznik stosu na ret
    jmp Koniec                                 ; przenosi do Koniec

nwdBlad:  
                                               ; wywoluje Blad
    call Blad

formatBlad: db "NWD jest wyliczane tylko dla liczb calkowitych dodatnich", 0xA, 0   ; wypisuje w przypadku, gdy ktoras z podanych wartosci jest 0


Blad:

    call [ebx + 3 * 4]     ; wywoluje printf w asmloader
    add esp, 1 * 4         ; ustawia wskaznik stosu na ret

Koniec:

    push 0
    call [ebx + 0 * 4]     ; exit